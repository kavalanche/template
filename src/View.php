<?php

declare(strict_types=1);

namespace Kavalanche\Template;

use Exception;

/**
 * @author Wojciech Burda <w.lk@wp.pl>
 */
class View {

    private string $templateDirectory;
    private string $defaultBaseTemplate;
    private array $globalData = [];

    public function __construct(string $templateDirectory, ?string $defaultBaseTemplate = null) {
        if (!file_exists($templateDirectory)) {
            throw new Exception('Template dorectory not found!');
        }
        $this->templateDirectory = $templateDirectory;
        if ($defaultBaseTemplate) {
            if (!file_exists($this->templateDirectory . '/' . $defaultBaseTemplate . '.php')) {
                throw new Exception('Base template not found!');
            }
            $this->defaultBaseTemplate = $defaultBaseTemplate;
        }
    }

    public function render(string $view, array $data = [], ?string $base = null) {
        extract($data);

        if (!file_exists($this->templateDirectory . '/' . $view . '.php')) {
            throw new Exception('Template not found!');
        }

        if ($base) {
            if (!file_exists($this->templateDirectory . '/' . $base . '.php')) {
                throw new Exception('Base template not found!');
            }
            ob_start();
            require $this->templateDirectory . '/' . $view . '.php';
            $content = ob_get_clean();

            require $this->templateDirectory . '/' . $base . '.php';
        } elseif ($this->defaultBaseTemplate) {
            ob_start();
            require $this->templateDirectory . '/' . $view . '.php';
            $content = ob_get_clean();

            require $this->templateDirectory . '/' . $this->defaultBaseTemplate . '.php';
        } else {
            require $this->templateDirectory . '/' . $view . '.php';
        }
    }

    public function return(string $view, array $data = [], ?string $base = null): string|false {
        extract($data);

        if (!file_exists($this->templateDirectory . '/' . $view . '.php')) {
            throw new Exception('Template not found!');
        }

        if ($base) {
            if (!file_exists($this->templateDirectory . '/' . $base . '.php')) {
                throw new Exception('Base template not found!');
            }
            ob_start();
            require $this->templateDirectory . '/' . $view . '.php';
            require $this->templateDirectory . '/' . $base . '.php';

            return ob_get_clean();
        } elseif ($this->defaultBaseTemplate) {
            ob_start();
            require $this->templateDirectory . '/' . $view . '.php';
            require $this->templateDirectory . '/' . $this->defaultBaseTemplate . '.php';

            return ob_get_clean();
        } else {
            ob_start();
            require $this->templateDirectory . '/' . $view . '.php';

            return ob_get_clean();
        }
    }

    public function addGlobalData(array $data) {
        foreach ($data as $k => $v) {
            $this->globalData[$k] = $v;
        }
    }
}

# Kavalanche/Template
Simple templating system

# Usage

1. Require kavalanche/template (composer)

    ```bash
    composer require kavalanche/template
    ```

2. Create instance of `Kavalanche\Template\View` somewhere in your front controller or other bootstrapping part of the code.

    ```php
    // without default base view defined
    $view = new Kavalanche\Template\View('/path/to/templates');
    // with default base view defined
    $view = new Kavalanche\Template\View('/path/to/templates', 'base');
    ```

3. Pass `$view` to your controller or wherever you want.

4. Invoke `render` method and pass variables to view file.

    ```php
    public function index() {
        $list = [...];
        // you can optionally override base view by passing third argument (e.g. 'newBase').
        $this->view->render('home/index', ['list' => $list]);
    }
    ```

5. Use your data inside view file.

    ```php
    foreach ($list as $item) {
        // whatever
    }
    ```

6. Remember that if you define base view, it must contain this or similar line od code:

    ```php
    echo $content;
    ```

# Changelog

## v0.3.0

Released 2019-10-27

- Makes data passed to view optional

## v0.2.0

Released 2019-10-19

- Added posibility to set base template 

## v0.1.0

Released 2019-10-19

- Initial release for testing purposes
